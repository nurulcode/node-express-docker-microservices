FROM node:20-alpine

LABEL maintainer="nurulspace0@gmail.com"
LABEL version="0.1"
LABEL description="Api Gateway"

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .

EXPOSE 3001

ENV NODE_ENV=development
CMD ["npm", "start"]
