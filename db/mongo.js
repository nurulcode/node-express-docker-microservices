import mongoose from "mongoose";

const mongoDB = async (req, res) => {
    try {
        await mongoose.connect(process.env.URL_MONGODB);
        console.log("Mongodb is connected!!!");
    } catch (error) {
        console.log(error);
    }
};

export default mongoDB;
