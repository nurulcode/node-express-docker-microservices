import redisClient from "../db/redis.js";

export const getRedisClient = async (key) => {
    return await redisClient.get(key);
};

export const setRedisClient = async (key, value, exp) => {
    return await redisClient.setEx(key, exp ,JSON.stringify(value))
};