import express from "express";
import dotenv from "dotenv";
import client from "./db/config.js";
import mongoDB from "./db/mongo.js";
import User from "./models/users.model.js";
import { getRedisClient, setRedisClient } from "./utils/redis.util.js";
const app = express();
dotenv.config();

// middleware
mongoDB();
app.use(express.json());

app.get("/", (req, res) => {
    res.json(
        "api-gateway-service!, api-gateway-service is running "
    );
});

app.get("/users", async (req, res) => {
    try {
        const results = await client.query("SELECT * FROM users");
        res.status(200).json(results.rows);
    } catch (err) {
        res.status(500).json({ error: err });
    }
});
app.post("/users/mongo", async (req, res) => {
    try {
        User.create({
            name: req.body.name,
            email: req.body.email,
        });

        res.status(200).json("insert data successfully");
    } catch (err) {
        res.status(500).json({ error: err });
    }
});

app.get("/users/mongo", async (req, res) => {
    try {
        const results = await User.find();
        res.status(200).json(results);
    } catch (err) {
        res.status(500).json({ error: err });
    }
});

app.get("/users/redis", async (req, res) => {
    try {
        if (!await getRedisClient("users")) {
            await new Promise((resolve) => setTimeout(resolve, 2000));
            const users = await User.find({});
            await setRedisClient('users', users, 10);
        }

        const results = await getRedisClient("users");
        res.status(200).json(JSON.parse(results));
    } catch (err) {
        res.status(500).json({ error: err });
    }
});


const port = process.env.PORT || 3001;
app.listen(port, function () {
    console.log(
        `app listening on port ${port}, api-gateway-service is running`
    );
});
